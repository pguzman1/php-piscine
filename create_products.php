<?php
include("admin_functions.php");

$unix = array("Minishell" => 300, "21sh" => 500, "42sh" => 1000);
$algo = array("Push_swap" => 300, "Lem_in"=>400, "Corewar" =>500);
$web = array("Camagru" => 250, "Matcha"=> 500, "Hypertube" => 700); // subcategory ????????????????????????????????????????????
$graphic = array("Fdf" =>200, "Wolf3d"=>300, "Rt"=>500);
$unix_t1 = array("Minishell" => 300);
$unix_t2 = array("21sh" => 500);
$unix_t4 = array("42sh" => 1000);
$algo_t1 = array("Push_swap" => 300);
$algo_t2 = array("Lem_in" => 400);
$algo_t4 = array("Corewar" => 500);


/*
foreach ($unix as $key => $value) {
  $array = array("unix");
  create_product($array, $key, $value);
}
*/
foreach ($unix_t1 as $key => $value) {
  $array = array("unix", "t1");
  create_product($array, $key, $value);
}
foreach ($unix_t2 as $key => $value) {
  $array = array("unix", "t2");
  create_product($array, $key, $value);
}
foreach ($unix_t4 as $key => $value) {
  $array = array("unix", "t4");
  create_product($array, $key, $value);
}

foreach ($algo_t1 as $key => $value) {
  $array = array("algo", "t1");
  create_product($array, $key, $value);
}
foreach ($algo_t2 as $key => $value) {
  $array = array("algo", "t2");
  create_product($array, $key, $value);
}
foreach ($algo_t4 as $key => $value) {
  $array = array("algo", "t4");
  create_product($array, $key, $value);
}
/*
foreach ($algo as $key => $value) {
  $array = array("algo");
  create_product($array, $key, $value);
}
*/
foreach ($web as $key => $value) {
  $array = array("web");
  create_product($array, $key, $value);
}
foreach ($graphic as $key => $value) {
  $array = array("graphic");
  create_product($array, $key, $value);
}
 ?>
