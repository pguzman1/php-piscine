<?php
session_start();
include ("connex.php");

if ( isset($_POST['submit']))
{
  $login = mysqli_real_escape_string($conn, htmlspecialchars(trim($_POST['login'])));
  $password = mysqli_real_escape_string($conn, htmlspecialchars(trim($_POST['password'])));
  $repeatpassword = mysqli_real_escape_string($conn, htmlspecialchars(trim($_POST['repeatpassword'])));
  if ($login && $password && $repeatpassword)
  {
    if ($password == $repeatpassword)
    {
      $password = hash('whirlpool', $password);
      $query = mysqli_query($conn, "SELECT * FROM USERS WHERE name = '$login'");
      if ($query)
      {
        $rows = mysqli_num_rows($query);
      }
      if ($rows == 0)
      {
        $id = uniqid();
        $sql = "INSERT INTO USERS (id, name, passwd) VALUES ('$id', '$login','$password')";
        $query = mysqli_query($conn, $sql);
        if (!$query)
          die("ERROR s :" . mysqli_error($conn));
        echo '<script language="Javascript"> document.location.replace("login.php")</script>';
      }
      else
        echo "Nom d'utilisateur deja pris";
    }
    else
    {
      include("error_login.php");
    }
  }
  else
      include("empty_field_error.php");
}
 ?>

<html>
<head>
  <meta charset="utf-8">
  <title>Register</title>
  <link rel="stylesheet" href="site.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
  <div class="wrap">
    <?php include("header.php") ?>
    <form method="post" action="register.php"
      <p>Identifiant: </p>
      <input type="text" name="login">
      <p>Mot de passe: </p>
      <input type="password" name="password">
      <p>Retapez votre mot de passe: </p>
      <input type="password" name="repeatpassword"><br/><br/>
      <input type="submit" value="S'inscrire" name="submit"><br/>
      <p>Deja inscrit? Cliquez ci-dessous pour vous connecter!</p>
    </form>
      <form method="post" action="login.php" value="go to login">
        <input action="login.php" type="submit" value="Se connecter" name="direct_login">
    </form>
  </div>
</body></html>
