<?php
session_start();

include("connex.php");
include("panier_functions.php");

if (!isset($_SESSION[user_id]))
{
  $_SESSION[user_id] = uniqid();
  create_shopping_cart_client_offline();

}// create $_SESSION[shopcart] and $_SESSION[user_id];

?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="site.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="blocks.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div class="wrap">
      <?php include("header.php") ?></br>
      <div class="main">
      <?php include("main.php") ?>
      <?php include("footer.php"); ?>
    </div>

    </div>
  </body>
</html>
