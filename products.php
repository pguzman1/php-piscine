<?php
session_start();
include("panier_functions.php");
include("connex.php");
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Page d'accueil</title>
    <link rel="stylesheet" href="site.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="blocks.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div class="wrap">
      <?php include("header.php") ?></br>
      <div class="main">
      <?php include("products_list.php") ?>
    </div>
    </div>
  </body>
</html>
