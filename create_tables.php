<?php

$sql = "CREATE DATABASE DBrush";
if (mysqli_query($conn, $sql))
{
  echo "Database created\n";
}
else {
  echo "Error :" . mysqli_error($conn) . "\n";
}

$sql = "CREATE TABLE USERS (
id VARCHAR(50) PRIMARY KEY,
name VARCHAR(30) NOT NULL,
passwd VARCHAR(150) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table USERS was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}

$sql = "CREATE TABLE ORDERS (
id VARCHAR(50) PRIMARY KEY,
user_id VARCHAR(50) NOT NULL,
status VARCHAR(50) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table ORDERS was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}

$sql = "CREATE TABLE ORDER_ITEMS (
id VARCHAR(30) PRIMARY KEY,
order_id VARCHAR(30) NOT NULL,
product_id VARCHAR(30) NOT NULL,
quantity INT(6) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table ORDER_ITEMS was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}

$sql = "CREATE TABLE PRODUCTS (
id VARCHAR(30) PRIMARY KEY,
##catpro_id VARCHAR(30) NOT NULL,
name VARCHAR(30) NOT NULL,
price INT(6) NOT NULL,
link VARCHAR(50) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table PRODUCTS was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}

$sql = "CREATE TABLE CATEGORIES (
id VARCHAR(30) PRIMARY KEY,
name VARCHAR(30) NOT NULL,
link VARCHAR(40) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table CATEGORIES was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}



$sql = "CREATE TABLE CAT_PER_PRO (
id VARCHAR(30) PRIMARY KEY,
cat_id VARCHAR(30) NOT NULL,
product_id VARCHAR(150) NOT NULL
)";

mysqli_select_db($conn, "DBrush");
if (mysqli_query($conn, $sql))
{
  echo "Table CAT_PER_PRO was created\n";
}
else {
  echo "ERROR : " . mysqli_error($conn) . "\n";
}
?>
