<?php
session_start();

include("connex.php");
include("panier_functions.php");

if (isset($_POST[delete]))
{
  remove_shopping_cart_client_offline($_POST[delete]);
}
?>

<html>
  <head>
    <meta charset="utf-8">
    <title>Panier</title>
    <link rel="stylesheet" href="site.css" media="screen" title="no title" charset="utf-8">
    <link rel="stylesheet" href="blocks.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>
    <div class="wrap">
      <?php include("header.php") ?></br>
      <div class="main">
        <?php  if (isset($_SESSION[shopcart]))
                  include("panier_list.php") ?>
    </div>
    </div>
  </body>
</html>
